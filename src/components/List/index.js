import React from "react";

// const List = (props) => {
//   return (
//       <li>{props.name}</li>
//   );
// };

class List extends React.Component {
  render() {
    return <li>{this.props.name}</li>;
  }
}
export default List;
