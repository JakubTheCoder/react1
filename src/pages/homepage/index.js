import React from "react";
import List from "../../components/List";

const homepage = () => {
  return (
    <div>
      <List name="Salt" />
      <List name="Pepper" />
      <List name="Chilli" />
      <List name="Oregano" />
      <List name="Rosmary" />
    </div>
  );
};

export default homepage;
